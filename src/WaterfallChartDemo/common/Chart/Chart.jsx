import React from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import './theme.css';
const theme = {
    chartContainer: 'chartContainer',
    chart: 'chart',
};

function setupResizeHandler(handler) {
  d3.select(window).on('resize', handler);
}

function removeResizeHandler() {
  d3.select(window).on('resize', () => {});
}

class Chart extends React.PureComponent {
  constructor(props) {
    super(props);

    this.checkDataAndUpdateChart = this.checkDataAndUpdateChart.bind(this);
  }

  componentDidMount() {
    this.createChartArea();
    setupResizeHandler(this.checkDataAndUpdateChart);
    this.checkDataAndUpdateChart();
  }

  componentDidUpdate() {
    this.checkDataAndUpdateChart();
  }

  componentWillUnmount() {
    removeResizeHandler(this.checkDataAndUpdateChart);
  }

  createChartArea() {
    const { chartContainer } = this;

    this.svg = d3.select(chartContainer)
      .append('svg');

    this.chartArea = this.svg
      .append('g')
      .attr('class', theme.chart);
  }

  checkDataAndUpdateChart() {
    if (this.props.data.length > 0) {
      this.updateChart();
    }
  }

  updateChart() {
    this.calculateChartSize();
    this.calculateScales();
    this.updateChartSize();
    this.redrawChart();
  }

  calculateChartSize() {
    const { chartContainer } = this;
    const { margin } = this.props;

    const chartContainerWidthPx = chartContainer.clientWidth;
    const chartContainerHeightPx = chartContainer.clientHeight;

    const decNumberFormat = 10;
    const chartContainerWidth = parseInt(chartContainerWidthPx, decNumberFormat);
    const chartContainerHeight = parseInt(chartContainerHeightPx, decNumberFormat);

    this.chartWidth = chartContainerWidth - margin.left - margin.right;
    this.chartHeight = chartContainerHeight - margin.top - margin.bottom - 4;
    // TODO: find the reason of difference between heights svg and div (4px)
  }

  // eslint-disable-next-line class-methods-use-this
  calculateScales() {
    throw new Error('Error: Chart:calculateScales() You must override this method in child class!');
  }

  updateChartSize() {
    const { margin } = this.props;

    this.svg
      .attr('width', this.chartWidth)
      .attr('height', this.chartHeight)
      .style('margin', `${margin.top}px ${margin.right}px ${margin.bottom}px ${margin.left}px`);
  }

  // eslint-disable-next-line class-methods-use-this
  redrawChart() {
    throw new Error('Error: Chart:redrawChart() You must override this method in child class!');
  }

  render() {
    return (
      <div
        ref={(chartContainer) => { this.chartContainer = chartContainer; }}
        className={theme.chartContainer}
      />
    );
  }
}

Chart.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ).isRequired,
  margin: PropTypes.shape({
    top: PropTypes.number,
    right: PropTypes.number,
    bottom: PropTypes.number,
    left: PropTypes.number,
  }),
};

Chart.defaultProps = {
  margin: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
};

export default Chart;
