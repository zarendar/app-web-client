import React from 'react';
import { mount } from 'enzyme';
import Chart from '../Chart';
import theme from '../theme.css';

import {
  SIMPLE_DATA,
  DEFAULT_MARGIN,
  EMPTY_DATA,
} from './mocks';

describe('Component/Common/Chart(Unit)', () => {
  function renderComponent(
    {
      method = mount,
      data = EMPTY_DATA,
      margin = DEFAULT_MARGIN,
    } = {},
  ) {
    return method(
      <Chart
        data={data}
        margin={margin}
      />,
    );
  }

  it('should throw error on component mounting', () => {
    const mountedComponent = renderComponent.bind(this, { data: SIMPLE_DATA });
    expect(mountedComponent).toThrow(new Error('Error: Chart:calculateScales() You must override this method in child class!'));
  });

  it('should throw no error on mounting with empty data', () => {
    expect(renderComponent).not.toThrow();
  });

  it('should render basic chart area', () => {
    const component = renderComponent();
    const divs = component.find('div');
    const chartParent = divs.at(0);
    expect(divs.length).toBe(1);
    expect(chartParent.hasClass(theme.chartContainer)).toBeTruthy();
  });
});
