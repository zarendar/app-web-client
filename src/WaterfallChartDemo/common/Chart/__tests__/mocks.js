export const EMPTY_DATA = [];
export const SIMPLE_DATA = [
  { id: '1' },
  { id: '2' },
  { id: '3' },
];

export const DEFAULT_MARGIN = {
  top: 0,
  right: 0,
  bottom: 0,
  left: 0,
};
