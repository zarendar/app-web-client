import * as colors from '../../colors.css';

export const MIN_BAR_HEIGHT = 1;
export const TRANSITION_DURATION = 500;
export const BAR_PADDING = 0.05;
export const BAR_BOTTOM_PADDING = 10;
export const DEFAULT_BAR_CONFIG = {
  isHighlighted: false,
  backgroundColor: colors.calypso,
  stroke: colors.silver,
  strokeDasharray: '0',
  strokeWidth: 0,
  text: '',
  textColor: colors.mystic,
  textFontSize: 16,
  textFontWeight: 'bold',
  textOuterColor: colors.tundora,
  label: '',
  labelColor: colors.silverChalice,
  labelFontSize: 14,
  highlightingColor: colors.blackSqueeze,
  onClick: null,
};
