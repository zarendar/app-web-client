import 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';
import Chart from '../Chart';
import renderers from './renderers';
import './theme.css';
import {
  BAR_PADDING,
  BAR_BOTTOM_PADDING,
  MIN_BAR_HEIGHT,
  TRANSITION_DURATION,
  DEFAULT_BAR_CONFIG,
} from './constants';

const theme = {
    bar: 'bar',
    clickable: 'clickable',
    axis: 'axis',
    highlighting: 'highlighting',
    barText: 'barText',
};

const {
  renderBars,
} = renderers;

class WaterfallChart extends Chart {
  createChartArea() {
    super.createChartArea();

    this.axisArea = this.svg
      .append('g')
      .attr('class', theme.axis);
    console.log(theme);
  }

  calculateScales() {
    const { chartWidth, chartHeight } = this;
    const { data, padding } = this.props;
    const {
      top: topPadding,
      right: rightPadding,
      bottom: bottomPadding,
      left: leftPadding,
    } = padding;

    this.xScale = d3.scaleBand()
      .domain(data.map(element => element.id))
      .range([leftPadding, chartWidth - rightPadding])
      .paddingInner(BAR_PADDING)
      .paddingOuter(BAR_PADDING);

    this.yScale = d3.scaleLinear()
      .domain([
        d3.min(data, item => d3.min([item.startValue, item.endValue])),
        d3.max(data, item => d3.max([item.startValue, item.endValue])),
      ])
      .range([chartHeight - bottomPadding, topPadding]);
  }

  redrawChart() {
    this.renderHighlighting();
    this.renderBars();
    this.renderAxis();
    this.renderText();
  }

  renderHighlighting() {
    const {
      chartArea,
      xScale,
      chartHeight,
    } = this;
    const { data, defaultConfig } = this.props;

    const highlightedBars = data.filter(item => item.isHighlighted || defaultConfig.isHighlighted);

    const bars = chartArea
      .selectAll(`.${theme.highlighting}`)
      .data(highlightedBars, (item, index) => item.id || index);

    const newBars = bars.enter()
      .append('g')
      .attr('class', theme.highlighting);

    bars.exit().remove();

    newBars
      .append('rect')
      .attr('x', item => xScale(item.id))
      .attr('y', 0)
      .attr('width', xScale.bandwidth())
      .attr('height', chartHeight)
      .attr('fill', item => item.highlightingColor || defaultConfig.highlightingColor);

    bars
      .select('rect')
      .transition(TRANSITION_DURATION)
      .attr('x', item => xScale(item.id))
      .attr('width', xScale.bandwidth())
      .attr('height', chartHeight)
      .attr('fill', item => item.highlightingColor || defaultConfig.highlightingColor);
  }

  renderBars() {
    const {
      chartArea,
      xScale,
      yScale,
    } = this;
    const { data, defaultConfig } = this.props;

    renderBars({
      selection: chartArea,
      xScale,
      yScale,
      data,
      classNames: theme,
      defaultConfig,
      transitionDuration: TRANSITION_DURATION,
      minBarHeight: MIN_BAR_HEIGHT,
    });
  }

  renderAxis() {
    const { xScale, yScale, axisArea } = this;
    const { data, defaultConfig } = this.props;
    const [yMax] = yScale.range();

    const labels = axisArea
      .selectAll('foreignObject')
      .data(data, (item, index) => item.id || index);

    const newLabels = labels
      .enter()
      .append('foreignObject');

    labels.exit().remove();

    const updateLabelsSelection = (selection) => {
      selection
        .attr('width', xScale.bandwidth())
        .attr('height', yMax)
        .attr('x', item => xScale(item.id))
        .attr('y', yMax + BAR_BOTTOM_PADDING)
        .style('font-size', item => item.labelFontSize || defaultConfig.labelFontSize)
        .style('color', item => item.labelColor || defaultConfig.labelColor)
        .style('text-align', 'center')
        .html(item => item.label || defaultConfig.label);
    };

    updateLabelsSelection(newLabels);
    updateLabelsSelection(labels);
  }

  renderText() {
    const { xScale, yScale, chartArea } = this;
    const { data, defaultConfig } = this.props;

    const isTextOuter = (item) => {
      const barHeight = Math.max(
        Math.abs(yScale(item.endValue) - yScale(item.startValue)), MIN_BAR_HEIGHT,
      );
      const fontSize = item.textFontSize || defaultConfig.textFontSize;

      return barHeight < fontSize;
    };

    const getTextYPosition = (item) => {
      const startPosition = Math.min(item.startValue, item.endValue);
      const yDifference = Math.abs(item.endValue - item.startValue) / 2;
      const fontSize = item.textFontSize || defaultConfig.textFontSize;

      const innerTextYPosition = yScale(startPosition + yDifference);
      const outerTextYPosition = yScale(startPosition - fontSize) + BAR_BOTTOM_PADDING;
      return isTextOuter(item) ? outerTextYPosition : innerTextYPosition;
    };

    const getTextColor = item => (isTextOuter(item)
      ? item.textOuterColor || defaultConfig.textOuterColor
      : item.textColor || defaultConfig.textColor
    );

    const texts = chartArea
      .selectAll('text')
      .data(data, (item, index) => item.id || index);

    const newTexts = texts
      .enter()
      .append('text')
      .classed(theme.clickable, item => item.onClick)
      .on('click', item => item.onClick());

    texts.exit().remove();

    const updateTextsSelection = (selection) => {
      selection
        .text(item => item.text || defaultConfig.text)
        .attr('x', item => xScale(item.id) + (xScale.bandwidth() / 2))
        .attr('y', getTextYPosition)
        .attr('fill', getTextColor)
        .attr('text-anchor', 'middle')
        .attr('alignment-baseline', 'central')
        .attr('font-size', item => item.textFontSize || defaultConfig.textFontSize)
        .attr('font-weight', item => item.textFontWeight || defaultConfig.textFontWeight);
    };

    updateTextsSelection(newTexts);
    updateTextsSelection(texts);
  }
}

WaterfallChart.propTypes = {
  defaultConfig: PropTypes.shape({
    isHighlighted: PropTypes.bool,
    backgroundColor: PropTypes.string,
    stroke: PropTypes.string,
    strokeDasharray: PropTypes.string,
    strokeWidth: PropTypes.number,
    text: PropTypes.string,
    textColor: PropTypes.string,
    textFontSize: PropTypes.number,
    textFontWeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    textOuterColor: PropTypes.string,
    label: PropTypes.string,
    labelColor: PropTypes.string,
    labelFontSize: PropTypes.number,
    highlightingColor: PropTypes.string,
    onClick: PropTypes.func,
  }),
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      isHighlighted: PropTypes.bool,
      backgroundColor: PropTypes.string,
      stroke: PropTypes.string,
      strokeDasharray: PropTypes.string,
      strokeWidth: PropTypes.number,
      text: PropTypes.string,
      textColor: PropTypes.string,
      textFontSize: PropTypes.number,
      textFontWeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      textOuterColor: PropTypes.string,
      startValue: PropTypes.number.isRequired,
      endValue: PropTypes.number.isRequired,
      label: PropTypes.string,
      labelColor: PropTypes.string,
      highlightingColor: PropTypes.string,
      onClick: PropTypes.func,
    }),
  ).isRequired,
  padding: PropTypes.object,
};

WaterfallChart.defaultProps = {
  ...Chart.defaultProps,
  padding: {
    top: 20,
    right: 0,
    bottom: 60,
    left: 0,
  },
  defaultConfig: DEFAULT_BAR_CONFIG,
};

export default WaterfallChart;
