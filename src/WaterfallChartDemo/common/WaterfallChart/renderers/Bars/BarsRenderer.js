import * as d3 from 'd3';

/* function selectRect(selection, shouldAppendItems) {
  if (shouldAppendItems === true) {
    return selection.append('rect');
  }
  return selection.select('rect');
} */

/* function updateBarsSelection({
  selection,
  classNames,
  xScale,
  yScale,
  defaultConfig,
  transitionDuration = 0,
  minBarHeight = 1,
  shouldAppendItems = false,
}) {
  selectRect(selection, shouldAppendItems)
    .attr('x', item => xScale(item.id))
    .attr('y', item => (
      shouldAppendItems
        ? yScale(d3.min([item.startValue, item.endValue]))
        : yScale(d3.max([item.startValue, item.endValue]))
    ))
    .attr('width', xScale.bandwidth())
    .attr('height', item => (
      shouldAppendItems
        ? 0
        : Math.max(Math.abs(yScale(item.startValue) - yScale(item.endValue)), minBarHeight)
    ))
    .attr('fill', item => item.backgroundColor || defaultConfig.backgroundColor)
    .style('stroke', item => item.stroke || defaultConfig.stroke)
    .style('stroke-dasharray', item => item.strokeDasharray || defaultConfig.strokeDasharray)
    .classed(classNames.clickable, item => item.onClick)
    .on('click', item => item.onClick && item.onClick())
    .transition()
    .duration(transitionDuration)
    .attr('y', item => yScale(d3.max([item.startValue, item.endValue])))
    .attr('height', item =>
      Math.max(Math.abs(yScale(item.startValue) - yScale(item.endValue)), minBarHeight),
    )
    .style('stroke-width', item => item.strokeWidth || defaultConfig.strokeWidth);
} */

function createNewBars({
  selection,
  xScale,
  yScale,
  defaultConfig,
  classNames,
  transitionDuration = 0,
  animationDelay = 0,
  minBarHeight = 1,
}) {
  selection
    .append('rect')
    .classed(classNames.clickable, item => item.onClick)
    .on('click', item => item.onClick())
    .attr('x', item => xScale(item.id))
    .attr('y', item => yScale(d3.min([item.startValue, item.endValue])))
    .attr('width', xScale.bandwidth())
    .attr('height', 0)
    .attr('fill', item => item.backgroundColor || defaultConfig.backgroundColor)
    .style('stroke', item => item.stroke || defaultConfig.stroke)
    .style('stroke-dasharray', item => item.strokeDasharray || defaultConfig.strokeDasharray)
    .style('stroke-width', item => item.strokeWidth || defaultConfig.strokeWidth)
    .transition()
    .duration(transitionDuration)
    .delay(animationDelay)
    .attr('y', item => yScale(d3.max([item.startValue, item.endValue])))
    .attr('height', item =>
      Math.max(Math.abs(yScale(item.startValue) - yScale(item.endValue)), minBarHeight),
    );
}

function updateExistingBars({
  selection,
  xScale,
  yScale,
  defaultConfig,
  classNames,
  transitionDuration = 0,
  animationDelay = 0,
  minBarHeight = 1,
}) {
  selection
    .select('rect')
    .classed(classNames.clickable, item => item.onClick)
    .on('click', item => item.onClick())
    .attr('fill', item => item.backgroundColor || defaultConfig.backgroundColor)
    .transition()
    .duration(transitionDuration)
    .delay(animationDelay)
    .attr('x', item => xScale(item.id))
    .attr('y', item => yScale(d3.max([item.startValue, item.endValue])))
    .attr('width', xScale.bandwidth())
    .attr('height', item =>
      Math.max(Math.abs(yScale(item.startValue) - yScale(item.endValue)), minBarHeight),
    )
    .style('stroke', item => item.stroke || defaultConfig.stroke)
    .style('stroke-dasharray', item => item.strokeDasharray || defaultConfig.strokeDasharray)
    .style('stroke-width', item => item.strokeWidth || defaultConfig.strokeWidth);
}

export default function renderBars({
  selection,
  xScale,
  yScale,
  data,
  defaultConfig,
  classNames,
  transitionDuration,
  animationDelay,
  minBarHeight,
}) {
  const barsConfig = {
    xScale,
    yScale,
    defaultConfig,
    classNames,
    transitionDuration,
    animationDelay,
    minBarHeight,
  };

  const bars = selection
    .selectAll(`.${classNames.bar}`)
    .data(data, (item, index) => item.id || index);

  const newBars = bars.enter()
    .append('g')
    .attr('class', classNames.bar);

  console.log(bars.exit());
  bars.exit().remove();

  createNewBars({ selection: newBars, ...barsConfig });
  updateExistingBars({ selection: bars, ...barsConfig });
}
