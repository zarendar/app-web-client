import * as d3 from 'd3';
import renderBars from '../BarsRenderer';

describe('BarsRenderer', () => {
  describe('should render bars correctly', () => {
    function expectCorrectRender(props) {
      const svg = document.createElement('svg');
      const selection = d3.select(svg);

      const {
        data,
        padding,
        defaultConfig,
        classNames,
      } = props;
      const {
        top: topPadding = 0,
        right: rightPadding = 0,
        bottom: bottomPadding = 0,
        left: leftPadding = 0,
      } = padding;

      const xScale = d3.scaleBand()
        .domain(data.map(element => element.id))
        .range([leftPadding, 1000 - rightPadding]);

      const yScale = d3.scaleLinear()
        .domain([
          d3.min(data, item => d3.min([item.startValue, item.endValue])),
          d3.max(data, item => d3.max([item.startValue, item.endValue])),
        ])
        .range([1000 - bottomPadding, topPadding]);

      renderBars({
        selection,
        xScale,
        yScale,
        data,
        defaultConfig,
        classNames,
        transitionDuration: 0,
        minBarHeight: 1,
      });


    }

    it('- 0 data points', () => {
      expectCorrectRender({
        data: [],
        defaultConfig: {
          backgroundColor: '#000',
          stroke: '#111',
          strokeDasharray: '0',
          strokeWidth: 0,
          onClick: null,
        },
        classNames: {
          bar: 'bar',
          clickable: 'clickable',
          axis: 'axis',
          highlighting: 'highlighting',
          barText: 'barText',
        },
      });
    });

    it('- 5 data points', () => {
      expectCorrectRender({ data: DATA.fiveDataPoints });
    });

    it('- 10 data points', () => {
      expectCorrectRender({ data: DATA.tenDataPoints });
    });
  });
});
