import React from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import WaterfallChart from '../WaterfallChart';
import theme from '../../Chart/theme.css';

import {
  CLASSNAMES,
  DATA,
  DEFAULT_MARGIN,
} from './mocks';

describe('WaterfallChart', () => {
  function renderComponent(
    {
      method = mount,
      data = DATA.emptyData,
      margin = DEFAULT_MARGIN,
    } = {},
  ) {
    return method(
      <WaterfallChart
        data={data}
        margin={margin}
      />,
    );
  }

  it('should receive correct props', () => {
    const { propTypes } = WaterfallChart;

    expect(propTypes.padding).toBe(PropTypes.object);
    expect(propTypes.defaultConfig).toBeDefined();
    expect(propTypes.data).toBeDefined();
  });

  it('should render basic chart area', () => {
    expect(renderComponent).not.toThrow();
    const component = renderComponent();
    const divs = component.find('div');
    const chartParent = divs.at(0);
    expect(divs.length).toBe(1);
    expect(chartParent.hasClass(theme.chartContainer)).toBeTruthy();
  });

  describe('should render waterfall chart correctly', () => {
    function expectCorrectRender(props) {
      const component = renderComponent(props);
      const { svg } = component.instance();

      const { data } = props;
      const dataLength = data.length;

      const bars = svg.selectAll(`.${CLASSNAMES.bar}`);
      expect(bars.size()).toBe(dataLength);

      const axisText = svg.selectAll('foreignObject');
      expect(axisText.size()).toBe(dataLength);

      const highlightedBars = data.filter(dataPoint => dataPoint.isHighlighted);
      const highlightedBarsLength = highlightedBars.length;
      const highlighting = svg.selectAll(`.${CLASSNAMES.highlighting}`);
      expect(highlighting.size()).toBe(highlightedBarsLength);
    }

    it('- 0 data points', () => {
      expectCorrectRender({ data: DATA.emptyData });
    });

    it('- 5 data points', () => {
      expectCorrectRender({ data: DATA.fiveDataPoints });
    });

    it('- 10 data points', () => {
      expectCorrectRender({ data: DATA.tenDataPoints });
    });
  });
});
