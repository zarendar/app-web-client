import React from 'react';
import { shallow } from 'enzyme';
import WaterfallChartWidget from '../WaterfallChartWidget';

describe('Components/Widgets/WaterfallChartWidget', () => {
  function renderComponent() {
    return shallow(<WaterfallChartWidget />);
  }

  it('should renders without crashing', () => {
    renderComponent();
  });
});
