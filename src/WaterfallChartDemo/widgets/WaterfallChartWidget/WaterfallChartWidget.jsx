import React from 'react';
import WaterfallChart from '../../common/WaterfallChart';

class CasesTableWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };

    this.setData = this.setData.bind(this);
  }

  setData(data) {
    this.setState({ data });
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <div style={{ width: '1000px', height: '500px', border: '#000 1px solid' }}>
          <WaterfallChart
            data={data}
          />
        </div>
        <hr />
        <h5>Value shared example:</h5>
        <div>
          <button
            onClick={() => {
              this.setData([
                {
                  id: 'eg1',
                  text: '1,800',
                  startValue: 0,
                  endValue: 1000,
                  label: 'NBA xyz price',
                },
                {
                  id: 'eg2',
                  backgroundColor: '#72d33b',
                  startValue: 1000,
                  endValue: 1190,
                  text: '190',
                  label: 'Increased production reduced',
                  onClick: () => alert('Hi, i\'m second bar'),
                },
                {
                  id: 'eg3',
                  backgroundColor: '#72d33b',
                  startValue: 1190,
                  endValue: 2170,
                  text: '180',
                  label: 'Lower final product price',
                },
                {
                  id: 'eg3',
                  backgroundColor: '#da0724',
                  startValue: 2170,
                  endValue: 2152,
                },
                {
                  id: 'eg4',
                  startValue: 0,
                  endValue: 2170,
                  text: '2,170',
                  label: 'Value price',
                },
                {
                  id: 'eg5',
                  backgroundColor: '#da0724',
                  startValue: 2170,
                  endValue: 2152,
                  text: '18',
                  label: 'Value shared to customer',
                },
                {
                  id: 'eg6',
                  startValue: 0,
                  endValue: 2152,
                  text: '2,152',
                  label: 'Minimum price',
                },
                {
                  id: 'eg7',
                  backgroundColor: '#72d33b',
                  startValue: 2152,
                  endValue: 1900,
                  text: '252',
                  label: 'Value captured',
                },
                {
                  id: 'eg8',
                  startValue: 0,
                  endValue: 1900,
                  text: '1,900',
                  label: 'Current price',
                },
              ]);
            }}
          >
            Show
          </button>
          <br /><br />
          <hr />
          <h5>Expand/collapse example</h5>
          <button
            onClick={() => {
              this.setData([
                {
                  id: '1',
                  text: '1,800',
                  startValue: 0,
                  endValue: 1000,
                  label: 'NBA xyz price',
                },
                {
                  id: '2',
                  backgroundColor: '#72d33b',
                  startValue: 1000,
                  endValue: 1500,
                  text: '500',
                  label: 'First expanded bar',
                  onClick: () => alert('Hi, i\'m first bar'),
                },
                {
                  id: '2.1',
                  backgroundColor: '#72d33b',
                  startValue: 1500,
                  endValue: 1750,
                  text: '250',
                  label: 'Second expanded bar',
                  onClick: () => alert('Hi, i\'m second bar'),
                },
                {
                  id: '2.2',
                  backgroundColor: '#72d33b',
                  startValue: 1750,
                  endValue: 1800,
                  text: '50',
                  label: 'Third expanded bar',
                  onClick: () => alert('Hi, i\'m third bar'),
                },
                {
                  id: '2.3',
                  backgroundColor: '#72d33b',
                  startValue: 1800,
                  endValue: 2000,
                  text: '200',
                  label: 'Fourth expanded bar',
                  onClick: () => alert('Hi, i\'m fourth bar'),
                },
                {
                  id: '2.4',
                  backgroundColor: '#72d33b',
                  startValue: 2000,
                  endValue: 2170,
                  text: '170',
                  label: 'Fifth expanded bar',
                  onClick: () => alert('Hi, i\'m fifth bar'),
                },
                {
                  id: '4',
                  startValue: 0,
                  endValue: 2170,
                  text: '2,170',
                  label: 'Value price',
                },
                {
                  id: '5',
                  backgroundColor: '#da0724',
                  startValue: 2170,
                  endValue: 2152,
                  text: '18',
                  label: 'Value shared to customer',
                },
                {
                  id: '6',
                  startValue: 0,
                  endValue: 2152,
                  text: '2,152',
                  label: 'Minimum price',
                },
                {
                  id: '7',
                  backgroundColor: '#72d33b',
                  startValue: 2152,
                  endValue: 1900,
                  text: '252',
                  label: 'Value captured',
                },
                {
                  id: '8',
                  startValue: 0,
                  endValue: 1900,
                  text: '1,900',
                  label: 'Current price',
                },
              ]);
            }}
          >
            Expand data
          </button>

          <button
            onClick={() => {
              this.setData([
                {
                  id: '1',
                  text: '1,800',
                  startValue: 0,
                  endValue: 1000,
                  label: 'NBA xyz price',
                },
                {
                  id: '2',
                  backgroundColor: '#72d33b',
                  startValue: 1000,
                  endValue: 2170,
                  text: '1170',
                  label: 'Collapsed bar',
                  onClick: () => alert('Hi, i\'m collapsed bar'),
                },
                {
                  id: '4',
                  startValue: 0,
                  endValue: 2170,
                  text: '2,170',
                  label: 'Value price',
                },
                {
                  id: '5',
                  backgroundColor: '#da0724',
                  startValue: 2170,
                  endValue: 2152,
                  text: '18',
                  label: 'Value shared to customer',
                },
                {
                  id: '6',
                  startValue: 0,
                  endValue: 2152,
                  text: '2,152',
                  label: 'Minimum price',
                },
                {
                  id: '7',
                  backgroundColor: '#72d33b',
                  startValue: 2152,
                  endValue: 1900,
                  text: '252',
                  label: 'Value captured',
                },
                {
                  id: '8',
                  startValue: 0,
                  endValue: 1900,
                  text: '1,900',
                  label: 'Current price',
                },
              ]);
            }}
          >
            Collapse data
          </button>
          <br /><br />
          <hr />
          <h5>Plus button example</h5>
          <button
            onClick={() => {
              this.setData([
                {
                  id: '1111',
                  startValue: 0,
                  endValue: 900,
                  text: '900',
                  label: 'Label 111',
                },
                {
                  id: '2222',
                  startValue: 900,
                  endValue: 2000,
                  text: '⨁',
                  backgroundColor: '#fff',
                  stroke: '#a1a1a1',
                  textColor: '#a1a1a1',
                  textFontSize: '50px',
                  textFontWeight: 'normal',
                  strokeWidth: 1,
                  label: 'Label 222',
                  onClick: () => alert('Hi, i\'m plus button'),
                },
                {
                  id: '3333',
                  startValue: 2000,
                  endValue: 800,
                  text: '1200',
                  label: 'Label 333',
                },
                {
                  id: '4444',
                  startValue: 800,
                  endValue: 0,
                  text: 'Any text',
                  label: 'Label 444',
                },
              ]);
            }}
          >
            Show
          </button>
        </div>
      </div>
    );
  }
}

export default CasesTableWidget;
