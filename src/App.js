import React, { Component } from 'react';
import WaterfallChart from '../src/WaterfallChartDemo/widgets/WaterfallChartWidget'
import './App.css';

class App extends Component {
  render() {
    return (
      <WaterfallChart />
    );
  }
}

export default App;
